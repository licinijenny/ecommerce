package com.test.ecommerce.models;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ordersdetails")
public class OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne()
    @JoinColumn(name = "order_id")
    private Orders orders;  //FUNCIONA MAAAAAAL

    @ManyToOne()
    @JoinColumn(name = "product_id")
    private Product product;

    public OrderDetail(int id, Orders orders, Product product) {
        this.id = id;
        this.orders = orders;
        this.product = product;
    }

    public OrderDetail() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}

