package com.test.ecommerce.models;


import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy = "orders")
    private Set<OrderDetail> orderDetails;

    Date date;

    @ManyToOne()
    private User user;

    public Orders(Date date, User user) {
        this.date = date;
        this.user = user;
    }

    public Orders() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
