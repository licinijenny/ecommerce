package com.test.ecommerce.service;

import com.test.ecommerce.models.Orders;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderService {

    Orders saveOrder(Orders orders);
    List<Orders> getOrdersByUser(Long id);

}
