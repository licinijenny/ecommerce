package com.test.ecommerce.controllers;


import com.test.ecommerce.models.Product;
import com.test.ecommerce.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class AppController {

    @Autowired
    private ProductService productService;

    @GetMapping({"/","/login"})
    public String index() {
        return "index";
    }

    @GetMapping("/homepage")
    public String defaultAfterLogin(HttpServletRequest request) {
        if (request.isUserInRole("ROLE_ADMIN")) {
            return "redirect:admin";
        }
        return "redirect:user";
    }

    @GetMapping("/user")
    public String user() {
        return "redirect:/ecommerce";
    }


    @GetMapping("/admin")
    public String admin() {
        return "admin";
    }
}