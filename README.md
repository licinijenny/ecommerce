**DOCUMENTACIÓN DEL PROYECTO**

**BACKEND Y FRONTEND**

Para el desarrollo del backend del ecommerce he utilizado las siguientes tecnologías:

Framework
→ Spring Boot

Lenguaje de programación
→ Java 8

Base de datos
→ MySQL

Software
→ XAMPP

El frontend ha sido desarrollado utilizando:
→ html
→ css

**TIEMPO DEDICADO**

El tiempo que he dedicado al proyecto son 4 dias, que se han gestionado de la siguiente
forma

Primer día → Recopilación de información y escoger las herramientas adecuadas para
desarrollar la página. 5 horas.

Segundo día → Gestión carrito de compra para poder añadir y borrar productos,
implementación base de datos MySQL (backend). 8 horas.

Tercer día → Timeout deseado del carro de compra (a cada refresh de la página se puede
ver el tiempo de timeout actualizado por pantalla) e implementación login para el
administrador y el cliente. 8 horas.

Cuarto día → Generar el ejecutable, test de la aplicación y documentar el proyecto. 2 horas.
Tiempo total dedicado = 23 horas.

**UTILIZÓ APLICACIÓN**

Para poder ejecutar la aplicación es necesario abrir el terminal en la carpeta donde se
encuentra el archivo y escribir el siguiente comando: *

**java -jar ./ecommerce-0.0.1-SNAPSHOT,jar**

Con cualquier navegador ir al siguiente link
localhost:3306

Credenciales para acceder como cliente:
username: user
password: user

Credenciales para acceder como administrador:
username: admin
password: admin
*Asegurarse de no tener XAMPP abierto o cualquier aplicación que utilice el puerto 3306
